using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Playdarium.Localization.Runtime.Models;
using Playdarium.Serializer;
using Playdarium.SpreadsheetDownloader.Helpers;
using UnityEditor;
using UnityEngine;

namespace Playdarium.Localization
{
	[CustomEditor(typeof(LocalizationData))]
	public class LocalizationDataEditor : MultipleSpreadsheetDownloader
	{
		private SerializedProperty _pageIdSp;
		private LocalizationData _localizationData;

		protected override string[] PageIds => new[] { _pageIdSp.stringValue };

		private void OnEnable()
		{
			_localizationData = (LocalizationData)target;
			_pageIdSp = serializedObject.FindProperty("pageId");
		}

		public override void OnInspectorGUI()
		{
			if (GUILayout.Button("PrintCharSet"))
				PrintCharArray();

			base.OnInspectorGUI();
		}

		protected override void Serialize(Dictionary<string, string> jsonByTables)
		{
			var keys = SerializeTranslations(jsonByTables[_pageIdSp.stringValue]);

			foreach (var group in keys.GroupBy(s => s.Name))
			{
				if (group.Count() > 1)
					Debug.LogError($"[{nameof(LocalizationDataEditor)}] Has key duplicate '{group.Key}'");
			}

			var array = serializedObject.FindProperty("keys");
			SerializedPropertySerializer.SerializeValueProperty(keys, array);
		}

		private Key[] SerializeTranslations(string json) => ReadAsJArray(json)
			.Cast<JObject>()
			.Where(f => f.ContainsKey("key"))
			.Select(ToKey)
			.ToArray();

		private Key ToKey(JObject jObject)
		{
			var languagesMap = _localizationData.LanguagesMap;
			var values = new Value[languagesMap.Length];

			for (var i = 0; i < languagesMap.Length; i++)
			{
				var language = languagesMap[i];
				values[i] = new Value(language, jObject.Value<string>(language) ?? string.Empty);
			}

			return new Key { Name = jObject.Value<string>("key"), Values = values };
		}

		private void PrintCharArray()
		{
			var localizationData = (LocalizationData)target;

			var charsArray = localizationData.Keys
				.SelectMany(s => s.Values)
				.SelectMany(s => s.Text.ToCharArray())
				.SelectMany(s => new[] { char.ToUpper(s), char.ToLower(s) })
				.Concat("!\"#$%&'()*+,-./0123456789:>=<;?@[\\]^_~{|}")
				.Distinct()
				.ToArray();

			Debug.Log($"[{nameof(LocalizationDataEditor)}] {new string(charsArray)}");
		}

		[Serializable]
		public class KeyJson
		{
			[SerializeField] private string key;
			[SerializeField] private string[] languages;

			public bool IsEmpty() => string.IsNullOrEmpty(key);

			public Key ToKey(IEnumerable<string> languagesMap) => new()
			{
				Name = key,
				Values = languagesMap.Select((language, i) => new Value(language, languages[i])).ToArray()
			};
		}
	}
}