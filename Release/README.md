# Localization

[![NPM package](https://img.shields.io/npm/v/com.playdarium.unity.localization?logo=npm&logoColor=fff&label=NPM+package&color=limegreen)](https://www.npmjs.com/package/com.playdarium.unity.localization)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Installing

Using the native Unity Package Manager introduced in 2017.2, you can add this library as a package by modifying your
`manifest.json` file found at `/ProjectName/Packages/manifest.json` to include it as a dependency. See the example below
on how to reference it.

### Install via OpenUPM

The package is available on the [npmjs](https://www.npmjs.com/package/com.playdarium.unity.localization)
registry.

#### Add registry scope

```json
{
  "dependencies": {
    ...
  },
  "scopedRegistries": [
    {
      "name": "Playdarium",
      "url": "https://registry.npmjs.org",
      "scopes": [
        "com.playdarium.unity"
      ]
    }
  ]
}
```

#### Add package in PackageManager

Open `Window -> Package Manager` choose `Packages: My Regestries` and install package

### Install via GIT URL

```json
"com.playdarium.unity.localization": "https://gitlab.com/pd-packages/localization.git#upm"
```

# Usage

## Install localization

Create scriptable installer `Installers/Localization/LocalizationProjectInstaller` and assign it to your
**ProjectContext**

## Google spreadsheet structure

**key** - Has fixed name and can't be customized  
**language names** - Language names can be customized because stored as string value

**Example:**

| key      | en      | fr     | ... |
|----------|---------|--------|-----|
| language | English | French | ... |
| ...      | ...     | ...    | ... |

## Add localization component

Add component **LocalizationInjection** on your **GameObject** and assign **Components** what you need localize.
Script **LocalizationInjection** automatically collect all fields with **LocalizationAttribute** from assigned
components.

## Localize fields

### Localize static text:

For localize static text add attribute **LocalizationAttribute** to target field. Target field should be **private**
and **SerializeField** or **public**.

```csharp
public class SomeView : MonoBehaviour
{
    [Localization("localization.key")] 
    [SerializeField]
    private Text text;
}
```

### Localize dynamic text:

For dynamic localization you need provide text contained args pattern.
Args pattern you can create using **Localizable.ToArgs(params string[] args)**.

```csharp
public class SomeView : MonoBehaviour
{
    [Localization("localization.key", true)] 
    [SerializeField]
    private Text text;
}
```

Localization key can contain formatting arguments.

#### Localization keys:

| Keys        | Values     |
|-------------|------------|
| text.format | "{0}: {1}" |
| text.score  | "Score"    |

```csharp
public class SomeView : MonoBehaviour
{
    [Localization("text.format", true)] 
    [SerializeField]
    private Text textField;
}

...

// Assign text to field in your controller script
textField.text = Localizable.ToArgs("text.score", "100");

// Output: "Score: 100"
```
