using UniRx;

namespace Playdarium.Localization.Runtime
{
	public interface ILocalizationService
	{
		string[] AvailableLanguages { get; }

		IReactiveProperty<string> CurrentLanguage { get; }

		void SetLanguage(string language);

		void NextLanguage();

		void PreviousLanguage();

		string Find(string key);
	}
}