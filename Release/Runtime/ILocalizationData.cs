using Playdarium.Localization.Runtime.Models;

namespace Playdarium.Localization.Runtime
{
	public interface ILocalizationData
	{
		string[] Languages { get; }

		Key Find(string search);
	}
}