﻿using System;
using UniRx;
using UnityEngine;

namespace Playdarium.Localization.Runtime.Impls
{
	public class LocalizationService : ILocalizationService
	{
		private readonly ILanguageGroups _groups;
		private readonly ILocalizationProvider _localizationProvider;

		public string[] AvailableLanguages { get; }

		private readonly ReactiveProperty<string> _currentLanguageProperty;
		public IReactiveProperty<string> CurrentLanguage => _currentLanguageProperty;

		public LocalizationService(
			ILocalizationData data,
			ILanguageGroups groups,
			ILocalizationProvider localizationProvider
		)
		{
			_groups = groups;
			_localizationProvider = localizationProvider;
			AvailableLanguages = data.Languages;
			_currentLanguageProperty = new ReactiveProperty<string>(GetDefaultLanguage());
		}

		private string GetDefaultLanguage()
			=> _groups.FindSubstitution(AvailableLanguages, Application.systemLanguage.ToString());

		public void PreviousLanguage()
		{
			var index = Array.IndexOf(AvailableLanguages, CurrentLanguage.Value) - 1;
			if (index < 0)
				index += AvailableLanguages.Length;

			_currentLanguageProperty.Value = AvailableLanguages[index];
		}

		public void NextLanguage()
		{
			var index = Array.IndexOf(AvailableLanguages, CurrentLanguage.Value) + 1;
			_currentLanguageProperty.Value = AvailableLanguages[index % AvailableLanguages.Length];
		}

		public void SetLanguage(string language)
		{
			var index = Array.IndexOf(AvailableLanguages, language);
			_currentLanguageProperty.Value = index != -1 ? language : GetDefaultLanguage();
		}

		public string Find(string key) => _localizationProvider.Find(_currentLanguageProperty.Value, key);
	}
}