using UnityEngine;

namespace Playdarium.Localization.Runtime.Impls
{
	public class DeviceLanguageProvider : IDeviceLanguageProvider
	{
		public string Get() => Application.systemLanguage.ToString();
	}
}