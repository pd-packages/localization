namespace Playdarium.Localization.Runtime
{
	public interface ILanguageGroups
	{
		string FindSubstitution(string[] availableLanguages, string desire);

		string FindSubstitution(string desire);
	}
}