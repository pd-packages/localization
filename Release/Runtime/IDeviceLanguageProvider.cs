namespace Playdarium.Localization.Runtime
{
	public interface IDeviceLanguageProvider
	{
		string Get();
	}
}