using System.Collections.Generic;
using UnityEngine;

namespace Playdarium.Localization.Runtime.Models
{
	[CreateAssetMenu(menuName = "Localization/LocalizationData", fileName = "LocalizationData")]
	public class LocalizationData : ScriptableObject, ILocalizationData
	{
		[Tooltip("This is sheet list gid in web uri")] [SerializeField]
		private string pageId;

		[SerializeField] private string[] languagesMap;
		[SerializeField] private string[] languages;
		[SerializeField] private List<Key> keys;

		public string[] LanguagesMap => languagesMap;

		public string[] Languages => languages;

		public IEnumerable<Key> Keys => keys;

		public Key Find(string search)
		{
			for (var i = 0; i < keys.Count; i++)
			{
				var key = keys[i];
				if (key.Name != search)
					continue;

				return key;
			}

			return null;
		}
	}
}