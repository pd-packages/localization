using System;
using UnityEngine;

namespace Playdarium.Localization.Runtime.Models
{
	[CreateAssetMenu(menuName = "Localization/LanguageGroups", fileName = "LanguageGroups")]
	public class LanguageGroups : ScriptableObject, ILanguageGroups
	{
		[SerializeField] private LanguageGroup[] groups =
		{
			new()
			{
				Substitution = SystemLanguage.Russian.ToString(),
				Desire = new[]
				{
					SystemLanguage.Ukrainian.ToString(),
					SystemLanguage.Belarusian.ToString()
				}
			},
			new()
			{
				Substitution = SystemLanguage.German.ToString(),
				Desire = new[]
				{
					SystemLanguage.Danish.ToString(),
					SystemLanguage.Dutch.ToString()
				}
			}
		};

		public string FindSubstitution(string[] availableLanguages, string desire)
		{
			if (Array.IndexOf(availableLanguages, desire) != -1)
				return desire;
			var cycle = 10;
			while (cycle > 0)
			{
				desire = FindSubstitution(desire);
				if (Array.IndexOf(availableLanguages, desire) != -1)
					return desire;
				cycle--;
			}

			return SystemLanguage.English.ToString();
		}

		public string FindSubstitution(string desire)
		{
			for (int i = 0, j = groups.Length; i < j; i++)
				if (Array.IndexOf(groups[i].Desire, desire) != -1)
					return groups[i].Substitution;

			return SystemLanguage.English.ToString();
		}
	}
}