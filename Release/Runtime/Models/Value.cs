using System;
using UnityEngine;

namespace Playdarium.Localization.Runtime.Models
{
	[Serializable]
	public class Value
	{
		[SerializeField] private string language;
		[SerializeField] private string text;

		public string Language
		{
			get => language;
			set => language = value;
		}

		public string Text
		{
			get => text;
			set => text = value;
		}

		public Value(string language, string text)
		{
			this.language = language;
			this.text = text;
		}
	}
}