namespace Playdarium.Localization.Runtime
{
	public interface ILocalizationProvider
	{
		string Find(string language, string domainKey);

		bool Find(string language, string domainKey, out string value);
	}
}