# Changelog

---

## [v2.0.2](https://gitlab.com/pd-packages/localization/-/tags/v2.0.2)

### Changes

- ILocalizationService change method to `void SetLanguage(string language)`

---

## [v2.0.1](https://gitlab.com/pd-packages/localization/-/tags/v2.0.1)

### Changes

- Update Spreadsheets download package

### Fixed

- Resolve dependencies

---

## [v2.0.0](https://gitlab.com/pd-packages/localization/-/tags/v2.0.0)

### Changes

- Remove Unity enum SystemLanguage and use a string type

---

## [v1.0.11](https://gitlab.com/pd-packages/localization/-/tags/v1.0.11)

### Changes

- Update package tools

---

## [v1.0.10](https://gitlab.com/pd-packages/localization/-/tags/v1.0.10)

### Changes

- Move to GitLab

---

## [v1.0.9](https://github.com/Playdarium/localization/releases/tag/v1.0.9)

### Fixed

- Project installer

---

## [v1.0.8](https://github.com/Playdarium/localization/releases/tag/v1.0.8)

### Added

- Possible localize `LocalizationInjection` as component

---

## [v1.0.7](https://github.com/Playdarium/localization/releases/tag/v1.0.7)

### Added

- Method for manual localization using `ILocalizationService`

---

## [v1.0.6](https://github.com/Playdarium/localization/releases/tag/v1.0.6)

### Added

- Check exist a localization key in LocalizationInjection component
- Editor tools for download, check null static and clear null static

---

## [v1.0.5](https://github.com/Playdarium/localization/releases/tag/v1.0.5)

### Added

- Test button for static objects translates

---

## [v1.0.4](https://github.com/Playdarium/localization/releases/tag/v1.0.4)

### Fixed

- Static localized object in LocalizationInjection component

---

## [v1.0.3](https://github.com/Playdarium/localization/releases/tag/v1.0.3)

### Fixed

- Fix LocalizationAttribute constructor

---

## [v1.0.2](https://github.com/Playdarium/localization/releases/tag/v1.0.2)

### Added

- Post process localized text

---

## [v1.0.1](https://github.com/Playdarium/localization/releases/tag/v1.0.1)

### Fixed

- Change language in `LocalizationService` using methods `NextLanguage` and `PreviousLanguage`

---

## [v1.0.0](https://github.com/Playdarium/localization/releases/tag/v1.0.0)

### Added

- Project init

---

## [v0.0.0](https://github.com/Playdarium/localization/releases/tag/v0.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.
