namespace Playdarium.Localization.Runtime.Zenject
{
	public interface ILocalizableObject
	{
		void Localize(string language);
	}
}