using System;
using UnityEngine;

namespace Playdarium.Localization.Runtime.Models
{
	[Serializable]
	public class LanguageGroup
	{
		[SerializeField] private string[] desire;
		[SerializeField] private string substitution;

		public string[] Desire
		{
			get => desire;
			set => desire = value;
		}

		public string Substitution
		{
			get => substitution;
			set => substitution = value;
		}
	}
}